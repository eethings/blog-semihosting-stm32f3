#define USE_STDPERIPH_DRIVER
#include <stm32f30x.h>
#include <stm32f3_discovery.h>
#include "stm32f30x_it.h"
#include <stdio.h>

__IO uint32_t TimingDelay = 0;
void Delay(__IO uint32_t);

/*
 * Don't forget the extern C since we are in C++!!!
 * Forgetting this means we will get mysterious undefined
 * initialise_monitor_handles error in the linker stage.
 * Also we need --specs=rdimon.specs option passed into linker
 */
#ifdef __cplusplus
extern "C" {
#endif
  extern void initialise_monitor_handles(void);
  void TimingDelay_Decrement(void);
#ifdef __cplusplus
}
#endif

int main(void)
{
  initialise_monitor_handles();
  // hard coded to 72MHz, since I know my PLL configs
  if (SysTick_Config(72e6 / 10)) {
    while(1);
  }
  /* Initialize LEDs */
  STM_EVAL_LEDInit(LED3);
  STM_EVAL_LEDOff(LED3);
  while(1) {
    printf("toggling\n");
    STM_EVAL_LEDToggle(LED3);
    Delay(5);
  }
}
void Delay(__IO uint32_t nTime)
{
  TimingDelay = nTime;
  while(TimingDelay != 0);
}

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
    {
      --TimingDelay;
    }
}
#ifdef __cplusplus
}
#endif

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
    {
    }
}
#endif
